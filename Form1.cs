﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot;

namespace AtlanticWritersBidder
{
    public partial class Form1 : Form
    {
        private IWebDriver driver;
        private bool isRunning = true;
        private TelegramBotClient telegramBot;
        public Form1()
        {
            InitializeComponent();
            InitializeTelegramBot();
        }

        private void InitializeTelegramBot()
        {
            string botToken = "6795715882:AAFO1XNKqTfjMdrcTeDriQNQJn289zFtYv0";
            //string botToken = "6454930624:AAHBc8NTSunXUSwVNko7MC5z3BRFD-C229o";
            telegramBot = new TelegramBotClient(botToken);
        }

        private void InitializeDriver()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-infobars");
            options.AddArgument("--disable-popup-blocking");
            driver = new ChromeDriver(options);
        }

        private async void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            button2.Enabled = true;

            await Task.Run(async () =>
           {
               string email = "anneestherr45@gmail.com";
               string password = "Esther@2027";
               InitializeDriver();
               driver.Navigate().GoToUrl("https://app.atlanticwriters.com/login");
               WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
               IWebElement emailInput = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("email")));
               IWebElement passwordInput = driver.FindElement(By.Name("password"));
               emailInput.SendKeys(email);
               passwordInput.SendKeys(password);
               IWebElement button = driver.FindElement(By.XPath("//button[contains(@class, 'noselect')]"));
               button.Click();
               IWebElement notificationsIcon = wait.Until(ExpectedConditions.ElementExists(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']")));
               notificationsIcon.Click();
               IList<IWebElement> noSelectDivs = wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']/div[2]/div[@data-name='Main']/div[2]/div[@data-name='NotificationFilters']/div[@data-name='Filter icons']//div[@class='noselect']")));
               noSelectDivs[1].Click();
               noSelectDivs[3].Click();
               notificationsIcon.Click();

               while (isRunning)
               {
                   if (!isRunning)
                   {
                       driver.Quit();
                       break;
                   }
                   try
                   {

                       IWebElement svgElement = wait.Until(ExpectedConditions.ElementExists(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']/div[@data-name='Open list trigger']/div/div[@class='noselect']//*[local-name()='svg']")));
                       ReadOnlyCollection<IWebElement> pathElements = svgElement.FindElements(By.TagName("path"));
                       int pathCount = pathElements.Count;
                       //string innerHtml = svgElement.GetAttribute("innerHTML");
                       //Console.WriteLine(pathCount);

                       if (pathCount == 3)
                       {
                           IWebElement notificationsIconLoop = wait.Until(ExpectedConditions.ElementExists(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']")));
                           notificationsIconLoop.Click();
                           IWebElement notificationMessage = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']/div[2]/div[@data-name='Main']/div[3]/div[@data-name='Size']/div[@data-name='Notification']/div[2]/div[@data-name='Text']")));
                           IWebElement notificationLink = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//body//div[@data-name='Reamaze']//div[@data-name='Size']//div[@data-name='Size']//div[@class='header svelte-x26yja']//div[@slot='header']//div[@data-name='Notifications Wrapper']//div[@data-name='Size']//div[@data-name='NotificationList']/div[2]/div[@data-name='Main']/div[3]/div[@data-name='Size']/div[@data-name='Notification']/div[2]/div[@data-name='Date & Links']/div[2]/a")));
                           string innerHtml = notificationMessage.GetAttribute("innerHTML");
                           Console.WriteLine(innerHtml);
                           await telegramBot.SendTextMessageAsync("734426507", innerHtml);
                           notificationLink.Click();
                           System.Threading.Thread.Sleep(2000);
                           IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                           js.ExecuteScript("window.history.back()");

                       }
                   }
                   catch (Exception ex)
                   {
                       continue;
                   }
               }
           });
            startButton.Enabled = true;
            button2.Enabled = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            isRunning = false;
            driver.Quit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            isRunning = false;
            driver.Quit();
            this.Close();
        }
    }
}
