﻿namespace AtlanticWritersBidder
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);
    public class Root
    {
        public string messaging_product { get; set; }
        public string to { get; set; }
        public string type { get; set; }
        public Text text { get; set; }
    }

    public class Text
    {
        public bool preview_url { get; set; }
        public string body { get; set; }
    }


}
